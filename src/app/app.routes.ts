import { Routes } from '@angular/router';
import {MapPage} from './map/map.page';

export const routes: Routes = [
  {
    path: '', component: MapPage
  },
];
