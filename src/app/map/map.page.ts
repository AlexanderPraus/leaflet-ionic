import { Component } from '@angular/core';
import { IonHeader, IonToolbar, IonTitle, IonContent } from '@ionic/angular/standalone';
import * as L from 'leaflet';
import '@gnatih/leaflet.legend';
import 'leaflet.heat';
import { GATEWAYS } from '../../assets/gateways';


@Component({
  selector: 'app-map',
  templateUrl: 'map.page.html',
  styleUrls: ['map.page.scss'],
  standalone: true,
  imports: [IonHeader, IonToolbar, IonTitle, IonContent],
})
export class MapPage {

  map: any;
  gateways = GATEWAYS.gateways;
  layerControl: L.Control.Layers = L.control.layers();
  gatewayLayersControl: L.Control.Layers = L.control.layers({}, {}, {collapsed: false, position: 'bottomright'});
  gatewayLayer: L.LayerGroup = L.layerGroup();
  minZomm: number = 13;
  maxZoom: number = 18;
  gatewayData: any = [];
  heatmapData: [number,number,number, string][] = [];
  gradient = {0.52: 'blue', 0.64: 'lime', .76: 'yellow', 0.88: 'orange', 1.0: 'red'};
  
  constructor() {}
  
  OnInit() {
  }

  ionViewDidEnter(){
    
    // initialize map
    this.map = new L.Map('map', {
      minZoom: this.minZomm,
      maxZoom: this.maxZoom,
      maxBounds: L.latLngBounds([51.074625519670136, 7.976074218750001], [50.981668164844045, 7.72441864013672]),
      center: [51.0282, 7.8503],
      zoom: 15
    });

    // initialize array for gateway data
    for (var gateway in this.gateways){
      this.gatewayData[this.gateways[gateway].id] = {
        'name': this.gateways[gateway].name,
        'data': [],
        'marker': null,
        'circle': null
      };
    }

    // load data from file
    fetch('assets/test_data.json').then( res => 
      res.json()).then(json => {
        this.heatmapData = json;
        this.initHeatmap();
      }
    );

    // create legend
    var legend = new (L.Control as any).Legend({
      position: 'bottomleft',
      column: 2,
      title: 'Legende',
      legends:[{
        label: 'Gateway',
        type: 'image',
        url: '../../assets/gateway.svg'
      },
      {
        label: 'Erwarteter Radius',
        type: 'circle',
        radius: 24,
        color: 'gray',
        fillColor: 'gray',
        fillOpacity: 0.5
      }
    ],
      symbolWidth: 24,
      symbolHeigth: 24
    });

    legend.addTo(this.map);
    let legendNode = document.getElementsByClassName('leaflet-legend')[0].children[0];

    let descriptionNode = document.createElement('div');
    descriptionNode.innerHTML = `
      Diese Heatmap zeigt die Abdeckung des LoRaWAN-Netzes in der Region Olpe.
      Dabei wurde die Signalstärke (über ein sog. RSSI-Wert) an verschiednen Orten gemessen. \r\n
      Sehr gute Signalstärken werden rot angezeit während schlechte Singalstärken blau sind.
      Mit einem Click auf ein Gateway wird der erwartete Radius angezeigt. \r\n
      `;
    legendNode.insertBefore(descriptionNode, legendNode.children[1]);

    // add controls to map
    this.layerControl.addTo(this.map);
    this.gatewayLayersControl.addTo(this.map);
    
    
    this.initBasemap();
    this.initGateways();

    // Toggle gateway markers based on zoom
    this.map.on('zoom', ()=>{
      if(this.map.getZoom() > 13){
        this.gatewayLayer.addTo(this.map);
      }else{
        this.gatewayLayer.removeFrom(this.map);
      }
    });

    this.map.invalidateSize();
  }

  /**
   * initialize heatmap layers for rssi data
   */
  initHeatmap(){
    let heatLayer: L.HeatLayer;
    let heatdata: L.HeatLatLngTuple[] = [];

    // iterate over data
    for (var row in this.heatmapData){
      let entry: L.HeatLatLngTuple = [this.heatmapData[row][0], this.heatmapData[row][1], (250 - Math.abs(this.heatmapData[row][2]))/250];
      this.heatmapData[row][3] = String(this.heatmapData[row][3]).toLowerCase();

      heatdata.push(entry);

      // if gateway exists, add entry to gateway data
      if(this.gatewayData[this.heatmapData[row][3]]){
        this.gatewayData[this.heatmapData[row][3]].data.push(entry);
      } 
    }

    heatLayer = L.heatLayer(heatdata, {radius: 50,  gradient: this.gradient, blur: 100});
    this.gatewayLayersControl.addBaseLayer(heatLayer, "Alle");
    heatLayer.addTo(this.map);

    // create heatmap for each gateway
    for (var gateway in this.gatewayData){
      let gatewayHeat = L.heatLayer( this.gatewayData[gateway].data, {radius: 50, gradient: this.gradient, blur: 100});
      gatewayHeat.options['name'] = this.gatewayData[gateway].name;
      this.gatewayData['layer'] = gatewayHeat;
      this.gatewayLayersControl.addBaseLayer(gatewayHeat, this.gatewayData[gateway].name);

      // add marker and cricle layer of gateway when heat map is selected
      gatewayHeat.on('add', (event)=>{
          if (event.target.options.name){
            // find id to go with gateway name
            let id = this.gateways.filter((data)=>{
              // remove all circles from map
              this.map.removeLayer(this.gatewayData[data.id].circle);
              return data.name == event.target.options.name;
            })[0].id;

            // check that marker is added
            if (!this.map.hasLayer(this.gatewayData[id].marker)){
              this.map.addLayer(this.gatewayData[id].marker);
            }
            // check that circle is added
            if (!this.map.hasLayer(this.gatewayData[id].circle)){
              this.map.addLayer(this.gatewayData[id].circle);
            }
          }
      });

    }
    
  }
  
    /**
   * Add base layers to map
   */
  initBasemap(){
    var osm = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });
    this.layerControl.addBaseLayer(osm, 'OSM');

    var OpenStreetMap_HOT = L.tileLayer('https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Tiles style by <a href="https://www.hotosm.org/" target="_blank">Humanitarian OpenStreetMap Team</a> hosted by <a href="https://openstreetmap.fr/" target="_blank">OpenStreetMap France</a>'
    });
    this.layerControl.addBaseLayer(OpenStreetMap_HOT, 'OSM HOT');

    var Esri_WorldImagery = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
      attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
    });
    this.layerControl.addBaseLayer(Esri_WorldImagery, 'ESRI');

    osm.addTo(this.map);
  }

  /**
   * Add layer with marker for all gateways to map
   */
  initGateways(){
    var gatewayIcon = new L.Icon({
      iconUrl: "../../assets/gateway.svg",
      iconSize:     [38, 95],
      shadowSize:   [50, 64],
      shadowAnchor: [4, 62],
      popupAnchor:  [-3, -76]
    });
    for (var gateway in this.gateways) {
      let circle = L.circle([this.gateways[gateway].lat, this.gateways[gateway].lng], {radius: 500, color: 'gray', weight: 1});
      let layer = L.marker([this.gateways[gateway].lat, this.gateways[gateway].lng], {icon: gatewayIcon, title: this.gateways[gateway].name}).bindPopup(this.gateways[gateway].name);
      this.gatewayData[this.gateways[gateway].id]['marker'] = layer;
      this.gatewayData[this.gateways[gateway].id]['circle'] = circle;
      layer.on('click', () => {
        if(this.map.hasLayer(circle)){
          this.map.removeLayer(circle);
        }else{
          this.map.addLayer(circle);
        }
      });

      layer.on('remove',() => {
        if(this.map.hasLayer(circle)){
          this.map.removeLayer(circle);
        }
      });

      circle.on('click', ()=>{
        this.map.removeLayer(circle);
      });

      this.gatewayLayer.addLayer(layer);
    }

    this.layerControl.addOverlay(this.gatewayLayer, "Gateways");
    this.map.addLayer(this.gatewayLayer);
  }
}
