export const GATEWAYS =
{
    "gateways": [
        {
            "name": "scoe1-Rathaus",
            "id": "eui-7076ff0056050c36",
            "lat": 51.029006492420294,
            "lng": 7.842408680072794
        },
        {
            "name": "scoe2-sgo",
            "id": "eui-7076ff0056070abd",
            "lat": 51.0326096604207,
            "lng": 7.84514492383463
        },
        {
            "name": "scoe3-stadthalle",
            "id": "eui-7076ff0056070ab3",
            "lat": 51.030181338812945,
            "lng": 7.8512267636595325
        },
        {
            "name": "scoe4-gshohenstein",
            "id": "eui-7076ff0056070ac3",
            "lat": 51.027993868766536,
            "lng": 7.849511152527251
        },
        {
            "name": "scoe5-gallenberg",
            "id": "eui-7076ff0056070a64",
            "lat": 51.03322278401467,
            "lng": 7.85342018571905
        },
        {
            "name": "scoe6-gsrhode",
            "id": "eui-7076ff0056070ab7",
            "lat": 51.051430175382315,
            "lng": 7.8634700558167925
        },
        {
            "name": "scoe7-sekundarschule",
            "id": "eui-7076ff0056070a86",
            "lat": 51.032542523171124,
            "lng": 7.868697369372852
        },
        {
            "name": "scoe8-gsdueringer",
            "id": "eui-7076ff005607105b",
            "lat": 51.0196071703371,
            "lng": 7.836818894245142
        },
        {
            "name": "scoe9-gsdueringerdahl",
            "id": "eui-7076ff0056070fd4",
            "lat": 51.00267005790844,
            "lng": 7.846623422955371
        },
        {
            "name": "scoe10-R309",
            "id": "eui-5031395335744750",
            "lat": 51.0289986682052,
            "lng": 7.84233653159643
        }
    ]
};