FROM node:21-slim
WORKDIR /app
COPY package*.json /app/
RUN npm install -g @angular/cli
COPY ./ /app/
RUN npm install
EXPOSE 8100
# ENTRYPOINT ["ng"]
CMD ng serve --port=8100 --host=0.0.0.0 --live-reload=false --poll=4000 --disable-host-check
